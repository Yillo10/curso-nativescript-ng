import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { HoteldetalleComponent } from "./hoteldetalle.component";

const routes: Routes = [
    { path: "", component: HoteldetalleComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class HoteldetalleRoutingModule { }
