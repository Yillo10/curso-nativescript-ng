import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

//import { HotelesRoutingModule } from "./hoteles-routing.module";
import { HoteldetalleComponent } from "./hoteldetalle.component";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        //HotelesRoutingModule
    ],
    declarations: [
        HoteldetalleComponent
    ],
    // providers: [NoticiasService], 
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class HoteldetalleModule { }
