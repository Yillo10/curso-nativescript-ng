import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";




@Component({
    selector: "Hoteldetalle",
    templateUrl: "./Hoteldetalle.component.html",
   /* providers: [NoticiasService] */
})

export class HoteldetalleComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
              
     }
     
    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
  
}
