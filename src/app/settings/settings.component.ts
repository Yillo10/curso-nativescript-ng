import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toasts";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    doLater(fn) { setTimeout(fn, 1000);}


    ngOnInit(): void {
        // Init your component properties here.
        /*
        this.doLater(() => 
        dialogs.action("Mensaje", "Cancelar!", ["opcion 1", "opcion 2"])
        .then((result) => {
            console.log("Resultado: " + result);
          
            if (result === "opcion 1") {
                this.doLater(() => 
                dialogs.alert({
                    title: "Titulo 1",
                    message: "msje 1",
                    okButtonText: "btn 1"
                }).then(() => console.log("cerrado 1!")));
            }else if (result === "opcion 2") {
                this.doLater(() =>
                dialogs.alert({
                    title: "Titulo 2",
                    message: "msje 2",
                    okButtonText: "btn 2"
                }).then(() => console.log("cerrado 2!")));
            }
        }));
          */
        const toastOptions: Toast.ToastOptions = {text: "hello world", duration: Toast.DURATION.SHORT};
        this.doLater(() => Toast.show(toastOptions));
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
