import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../domain/noticias.service";
import { Color } from "tns-core-modules/color";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
   /* providers: [NoticiasService] */
})
export class SearchComponent implements OnInit {
    resultados: Array<string>;
    @ViewChild("layout") layout: ElementRef;
    
    constructor(public noticias: NoticiasService) {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
       this.noticias.agregar("Hola!");
       this.noticias.agregar("Hola 2!");
       this.noticias.agregar("Hola 3!"); 
       this.noticias.agregar("Hola 4!");          
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x): void{
        console.dir(x);
    }

    buscarAhora(s: string) {
        this.resultados= this.noticias.buscar().filter((x) => x.indexOf(s) >= 0);

        const layout = <view>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 3000,
            delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 3000,
            delay: 150
        }));
    }
}

