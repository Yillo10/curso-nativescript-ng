import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { HotelesRoutingModule } from "./hoteles-routing.module";
import { HotelesComponent } from "./hoteles.component";
import { HotelesService } from "../domain/hoteles.service";
import { HoteldetalleComponent } from "../hoteldetalle/hoteldetalle.component";


@NgModule({
    imports: [
        NativeScriptCommonModule,
        HotelesRoutingModule
    ],
    declarations: [
        HotelesComponent,
        HoteldetalleComponent
    ],
    // providers: [NoticiasService], 
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class HotelesModule { }
