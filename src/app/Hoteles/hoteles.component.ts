import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { HotelesService } from "../domain/hoteles.service";
import { Router, NavigationEnd } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { filter } from "rxjs/operators";

@Component({
    selector: "Hoteles",
    templateUrl: "./Hoteles.component.html",
   /* providers: [NoticiasService] */
})
export class HotelesComponent implements OnInit {
    private _activatedUrl: string;
    constructor(public hoteles: HotelesService,private router: Router, private routerExtensions: RouterExtensions) {
        // Use the component constructor to inject providers.
    }

 
    ngOnInit(): void {
       this.hoteles.agregar("Hotel Barceloa");
       this.hoteles.agregar("Hotel Paris");
       this.hoteles.agregar("Hotel Rio de Janeiro"); 
       this.hoteles.agregar("Hotel de Madrid");     
       
       this._activatedUrl = "/home";
        this.router.events
        .pipe(filter((event: any) => event instanceof NavigationEnd))
        .subscribe((event: NavigationEnd) => this._activatedUrl = event.urlAfterRedirects);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x): void{
        console.dir(x);
    }

    isComponentSelected(url: string): boolean {
        return this._activatedUrl === url;
    }

    onNavItemTap(navItemRoute: string): void {
        this.routerExtensions.navigate([navItemRoute], {
            transition: {
                name: "fade"
            }
        });

    }


}
