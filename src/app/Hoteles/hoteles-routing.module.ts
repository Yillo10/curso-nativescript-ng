import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { HotelesComponent } from "./hoteles.component";
import { HoteldetalleComponent } from "../hoteldetalle/hoteldetalle.component";

const routes: Routes = [
    { path: "", component: HotelesComponent },
    { path: "hoteldetalle", component: HoteldetalleComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class HotelesRoutingModule { }
