import { Injectable } from "@angular/core";

@Injectable()
//Administra el servicio de Hoteles
export class HotelesService {
    private hoteles: Array<string> = [];
    agregar (s: string) {
        this.hoteles.push(s);  
    }
    buscar() {
        return this.hoteles;
    }
}